#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import cgi
import logging
import sys
import webapp2

# Insert lib folder to path
sys.path.insert(0, "lib")
import bs4

from utils import render, retrieve_num_download_cached, send_feedback

class TemplateHandler(webapp2.RequestHandler):
    def get(self):
        render(self.response, self.template_name)

class MainHandler(webapp2.RequestHandler):
    def get(self):
        num_download = retrieve_num_download_cached()
        render(self.response, "index.jinja2", {"num_download": num_download})

class AboutHandler(TemplateHandler):
    template_name = "about.jinja2"

class ChangelogHandler(TemplateHandler):
    template_name = "changelog.jinja2"

class FeatureHandler(TemplateHandler):
    template_name = "feature.jinja2"

class EmailHandler(webapp2.RequestHandler):
    def post(self):
        send_feedback(self.request.get("sender"), cgi.escape(self.request.get("title")), cgi.escape(self.request.get("content")))
        self.response.out.write("")

class LatestHandler(webapp2.RequestHandler):
    def get(self):
        version = self.request.get("version")
        if version == "3.15.0":
            self.response.write("true")
        else:
            self.response.write("false")

app = webapp2.WSGIApplication([
    ("/", MainHandler),
    ("/feature", FeatureHandler),
    ("/about", AboutHandler),
    ("/changelog", ChangelogHandler),
    ("/email", EmailHandler),
    ("/latest", LatestHandler),
], debug=True)
