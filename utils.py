import bs4
import datetime
import jinja2
import logging
import os
import urllib2

from google.appengine.api import mail, memcache

JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.join(os.path.dirname(__file__), "templates")),
    extensions=["jinja2.ext.autoescape"])

def render(response, template_name, context = None):
    context = context if context != None else {}
    response.write(JINJA_ENVIRONMENT.get_template(template_name).render(context))

def send_feedback(sender, title, content):
    sender = sender.strip()
    admin_email = "ffmangareader@gmail.com"
    message = mail.EmailMessage(sender = admin_email,
                                to = admin_email,
                                subject = title)
    message.reply_to = sender if sender else admin_email
    message.body = """
        Sender-email: {sender}

{content}
    """.format(sender = sender, content = content).strip()
    message.send()

def retrieve_num_download():
    response = urllib2.urlopen("https://bitbucket.org/heryandi/firefoxmangareader_appengine/downloads").read()
    soup = bs4.BeautifulSoup(response)
    result = soup.select("table#uploaded-files")[0].select("td.count")

    total = sum(int(t.contents[0]) for t in result)
    return total

def retrieve_num_download_cached():
    cached = memcache.get("NUM_DOWNLOADED")
    if cached:
        return cached
    try:
        result = retrieve_num_download()
        memcache.add("NUM_DOWNLOADED", result, 1800)
    except:
        return None
    return result
